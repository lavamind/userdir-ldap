from unittest.mock import patch, mock_open

import pytest

import userdir_ldap.ssh as userdir_ssh
from userdir_ldap.exceptions import UDSkipLine, UDBadKey, UDFormatError

PUBKEY_DSA = "ssh-dss AAAAB3NzaC1kc3MAAACBAM6E+vR0lKxTIBxQIiOGFfQ/e2e1YqB2q1C4qXtS+h/Q7GLuBOKaRAchVQ3XkEublihs7UXgV2UTo2ltffi6Rvl4pc8B4mwzIrlv10xZq9sYaV1X0FH9hgXHnIS4Rk3bhN04OmaIYdOm32tKl+N3D6n0XhVpFQRG2DMpTWxg5013AAAAFQCAsaqyF18q417Awz65AYT3eAiNuQAAAIAiDM7k4LWX+erfdsuOGuS01qu4wM/KK9BU0aTmsJxBd1RyASM6a+MEskwzZaCmaDXxsO1tpjz/AzmJMPBERCgQQcMtDCpTB61vmxePUIgGuS7AQ7Zyp64pJ6vV/AGF11xrPwDcbg36lA1SxW7M6SSjiqvc2jwd4QA29o0vELaX3wAAAIEAxesNoYv5Rf13XuOviexu7ThI84uFLlU94WycHAU/UWKtshU0VG/OQlRjwFTxnJBJNeQhgQkiK7WFkQsgQpUVSOCo/YN7FUXPV7yQuvi9eM9dvQYdTAhekZKtYWFxXhVOiKILLjjIiIOm41CMDaY63aqmGzP6XlyU2ARXtN9ht4c="
PUBKEY_ECDSA = "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHTV5rRRznFfhRVXBBsU6y2HCE1twUtFn+VEbfUFma10yYgx+C+64dQDf923PK86Daw3sv2jTqjQj6DMLZfUDdE="
PUBKEY_RSA = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCj9jQQwZhyiCDXfEqK4d/RntKnJ/CuvU8nmwDL7WwvU7vjy3hS1ulvWN7h+H43bu+wbGYu/oWDb/6r7heMeH5AvAPV8QWY6H+7hcADYRO+tykuzu8dYGPLkN2tSzMgW5M+Z0pJrjEJlaUPGJxCJMvEK/NLtG15tAMY04d6OunlC3tRmq0J77E/3rMeWu+wUsYIK+SWfdsOCgkDAKQWTi4OemCuYUDsK5faWHBCISjsgfzx09vda/keL3CM3eHPKuE9kykpg3bq5rARh/UvaWKKQEubecYMio/me71a0ZqsbzzWzFndc4FNuWdtv47JIi7Fzml12pAcVBQwTPa3z6an0iC4hE+mHllwb5VFbPJpXzkbj3TgI0UdjxJa2k36zUp4l4sct3m9m4XIN5kEPB5Qzc5LV84dJYCh3ipmg3y74ley6OQdYbVLahyUQo8owETdZI6j/8CjRt9l/tR7h+pGoml7ngobVL4YMJN2fDhPHYsBHYdZhzZbdQfFmIPwKPU="
PUBKEY_RSA1024 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDitli46g3jSUUpczHXBuZfskp5hm1xADcHOsWKh7UR6qgCtyg8cl7+T+f/Rkvbv74MlCq9FvBpFxyEi3HWawv2MIWAgZ4B9B5zEVDe3SKJKsYavsmXbi7fgTvwsQrt9fK1G4Z1jv+tW7K18R8sNCwSs3ZHZ0nepKfV/oX2be4XEQ=="
FINGERPRINT_RSA = "SHA256:quOkA1ei+lRIgKUhT8wFQ1kztLfI9qv7cwohyn6Uu2E"
PUBKEY_ED25519 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP+ozlARE8crlk1lYg4HKAhGHSgZlncHfDn8Ij8K1Ygf"
FINGERPRINT_ED25519 = "SHA256:2Ii+D8o8wy1SByMjMyFF/M+nwFDhjSCKaDxZ9ktgbVg"
BADFINGERPRINTS = """00:00:c1:bb:09:d6:b2:ee:37:56:f1:7c:1d:ee:50:d8
00:01:13:58:cf:35:4a:c3:0b:96:80:d0:5e:99:be:b2
SHA256:ZThViAuslKxGDI6e+nROII5MAuGtrTXjjeAn+FaXzco
"""


@pytest.mark.parametrize(
    "key,expected",
    (
        (PUBKEY_DSA, "ssh-dss AAAAB3Nz..XtN9ht4c ="),
        (PUBKEY_ECDSA, "ecdsa-sha2-nistp256 AAAAE2Vj..MLZfUDdE ="),
        (PUBKEY_RSA, "ssh-rsa AAAAB3Nz..FmIPwKPU ="),
        (PUBKEY_ED25519, "ssh-ed25519 AAAAC3Nz..8Ij8K1Yg f"),
        ("not an ssh key", "<unknown format>"),
        (f"options {PUBKEY_RSA}", "options  ssh-rsa AAAAB3Nz..FmIPwKPU ="),
    ),
)
def test_format_ssh2_auth(key, expected):
    assert userdir_ssh.FormatSSH2Auth(key) == expected


def test_loadbadssh():
    with patch("userdir_ldap.ssh.open", mock_open(read_data=BADFINGERPRINTS)):
        assert userdir_ssh.LoadBadSSH("badfingerprints") == {
            "00:00:c1:bb:09:d6:b2:ee:37:56:f1:7c:1d:ee:50:d8",
            "MD5:00:00:c1:bb:09:d6:b2:ee:37:56:f1:7c:1d:ee:50:d8",
            "00:01:13:58:cf:35:4a:c3:0b:96:80:d0:5e:99:be:b2",
            "MD5:00:01:13:58:cf:35:4a:c3:0b:96:80:d0:5e:99:be:b2",
            "SHA256:ZThViAuslKxGDI6e+nROII5MAuGtrTXjjeAn+FaXzco",
        }


def test_sshparser():
    hostnames = {"somehost", "someotherhost"}
    with patch("userdir_ldap.ssh.open", mock_open(read_data=BADFINGERPRINTS)):
        parser = userdir_ssh.SSHParser("badfingerprints", hostnames)

    # reject DSA
    with pytest.raises(UDSkipLine):
        parser.parse(PUBKEY_DSA)
    # RSA-3072 OK
    assert parser.parse(PUBKEY_RSA) == ("3072", FINGERPRINT_RSA, PUBKEY_RSA)
    # Fingerprint on the reject list
    with pytest.raises(UDBadKey):
        parser.parse(PUBKEY_ECDSA)
    # ED25519 OK
    assert parser.parse(PUBKEY_ED25519) == ("256", FINGERPRINT_ED25519, PUBKEY_ED25519)
    # Key type alone on the line (wrapping)
    with pytest.raises(UDSkipLine):
        parser.parse("ssh-ed25519")
    # Not for us
    assert parser.parse("not an ssh key") is None
    # Don't start with the LDAP field name
    with pytest.raises(UDSkipLine):
        parser.parse(f"sshRSAAuthKey: {PUBKEY_ED25519}")
    # With allowed_hosts
    assert parser.parse(f"allowed_hosts=somehost {PUBKEY_ED25519}")[:2] == ("256", FINGERPRINT_ED25519)
    # Empty allowed_hosts list
    with pytest.raises(UDSkipLine):
        parser.parse(f"allowed_hosts= {PUBKEY_ED25519}")
    # Bad hostname format
    with pytest.raises(UDSkipLine):
        parser.parse(f"allowed_hosts=not_a_host {PUBKEY_ED25519}")
    # Unknown hostname
    with pytest.raises(UDSkipLine):
        parser.parse(f"allowed_hosts=unknown.host {PUBKEY_ED25519}")
    # Unknown option
    with pytest.raises(UDFormatError):
        parser.parse(f"options {PUBKEY_ED25519}")
    # Don't get confused between the key and comment
    assert parser.parse(f"{PUBKEY_ED25519} {PUBKEY_RSA}")[1] == FINGERPRINT_ED25519
    # RSA key too small
    with pytest.raises(UDFormatError):
        parser.parse(PUBKEY_RSA1024)
