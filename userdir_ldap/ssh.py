import os
import re
import subprocess
import tempfile

import sshpubkeys

from .exceptions import UDSkipLine, UDFormatError, UDBadKey, UDExecuteError


class UDSSHKey(sshpubkeys.SSHKey):
    RSA_MIN_LENGTH_STRICT = 2048


# SSH Key splitting. The result is:
# (options,size,modulus,exponent,comment)
SSHAuthSplit = re.compile(r'^(.* )?(\d+) (\d+) (\d+) ?(.+)$')
SSH2AuthSplit = re.compile(r'^(.* )?(ssh-(?:dss|rsa|ed25519)|ecdsa-sha2-nistp(?:256|384|521)) ([a-zA-Z0-9=/+]+) ?(.+)$')

SSHFingerprint = re.compile(r'^(\d+) ([0-9a-f\:]{47}|SHA256:[0-9A-Za-z/+]{43}) (.+)$')


def FormatSSH2Auth(Str):
    Match = SSH2AuthSplit.match(Str)
    if Match is None:
        return "<unknown format>"
    G = Match.groups()

    if G[0] is None:
        return "%s %s..%s %s" % (G[1], G[2][:8], G[2][-8:], G[3])
    return "%s %s %s..%s %s" % (G[0], G[1], G[2][:8], G[2][-8:], G[3])


def FormatSSHAuth(Str):
    Match = SSHAuthSplit.match(Str)
    if Match is None:
        return FormatSSH2Auth(Str)
    G = Match.groups()

    # No options
    if G[0] is None:
        return "%s %s %s..%s %s" % (G[1], G[2], G[3][:8], G[3][-8:], G[4])
    return "%s %s %s %s..%s %s" % (G[0], G[1], G[2], G[3][:8], G[3][-8:], G[4])


# Load bad ssh fingerprints
def LoadBadSSH(badfingerprints):
    bad = set()
    with open(badfingerprints, "r") as f:
        # Ideally the bad fingerprints file has the same format as what we want but
        # for legacy reasons right now it has the bare md5 fingerprints, so handle
        # both.
        FingerprintLine = re.compile(r'^([0-9a-f:]{47}).*$')
        for line in f.readlines():
            Match = FingerprintLine.match(line)
            if Match is not None:
                g = Match.groups()
                bad.add(g[0])
                bad.add("MD5:" + g[0])
            else:
                bad.add(line.strip())
    return frozenset(bad)


class SSHParser:
    ALLOWED_KEY_TYPES = ("ssh-rsa", "ssh-ed25519", "ecdsa-sha2-nistp256", "ecdsa-sha2-nistp384", "ecdsa-sha2-nistp521")
    machine_regex = re.compile("^[0-9a-zA-Z.-]+$")

    def __init__(self, badfingerprints, validhostnames):
        self.badkeys = LoadBadSSH(badfingerprints)
        self.validhostnames = validhostnames

    def parse(self, Str):
        # This list should really be a constant or method somewhere
        if Str.strip() in self.ALLOWED_KEY_TYPES:
            raise UDSkipLine("Key appears to have been line-wrapped. Each key must be on a single line. Using 'gpg --armor' can help avoid this issue.")

        Match = SSH2AuthSplit.match(Str)
        # If this looks nothing like an ssh key, return None so some other parser can try its luck
        if Match is None:
            return

        if Str.startswith("sshRSAAuthKey: "):
            raise UDSkipLine("invalid ssh key syntax; should not start with field name")

        machines = []
        if Str.startswith("allowed_hosts="):
            Str = Str.split("=", 1)[1]
            if ' ' not in Str:
                raise UDSkipLine("invalid ssh key syntax with machine specification")
            machines, Str = Str.split(' ', 1)
            machines = machines.split(",")
            for m in machines:
                if not m:
                    raise UDSkipLine("empty machine specification for ssh key")
                if not self.machine_regex.match(m):
                    raise UDSkipLine("machine specification for ssh key contains invalid characters")
                if m not in self.validhostnames:
                    raise UDSkipLine("unknown machine {} used in allowed_hosts stanza for ssh keys".format(m))

        try:
            key = UDSSHKey(Str, strict=True)
            key.parse()
        except Exception as e:
            raise UDFormatError(e)

        if key.key_type.decode("ascii") not in self.ALLOWED_KEY_TYPES:
            raise UDSkipLine("SSH key fails formal criteria, not added.")

        fingerprint = key.hash_sha256()
        if {fingerprint, key.hash_md5()} & self.badkeys:
            raise UDBadKey(fingerprint)

        (fd, path) = tempfile.mkstemp(".pub", "sshkeytry", "/tmp")
        f = open(path, "w")
        f.write("%s\n" % (Str))
        f.close()
        cmd = ["/usr/bin/ssh-keygen", "-l", "-f", path]
        proc = subprocess.run(cmd, stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
        os.remove(path)
        if (proc.returncode != 0):
            raise UDExecuteError("ssh-keygen -l invocation failed!\n%s\n" % (proc.stdout))
        Match = SSHFingerprint.match(proc.stdout)
        if Match is None:
            raise UDFormatError("Failed to match SSH fingerprint, has the output of ssh-keygen changed?")
        g = Match.groups()
        key_size = g[0]
        if g[1] != fingerprint:
            raise UDFormatError("Fingerprint from ssh-keygen different from expectation")

        return key_size, fingerprint, Str
